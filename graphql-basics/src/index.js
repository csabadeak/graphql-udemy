import { GraphQLServer } from 'graphql-yoga';
// String, Boolean, Int, Float, ID

// Type defintion (aka Schema)
const typeDefs = `
    type Query {
        hello: String!
        name:String!
        location: String!
        bio: String!
    }
`;

// Resolvers
const resolvers = {
    Query: {
        hello() {
            return 'This my first query!';
        },
        name(){
            return 'Csaba';
        },
        location(){
            return 'Budapest';
        },
        bio(){
            return 'My Name is Csaba';
        }
    }
}

const server = new GraphQLServer({
        typeDefs: typeDefs,
        resolvers: resolvers
    }
)

server.start(() => {
    console.log('Server is up');
})